# Favourite Car Colour Application README

## Objectives

This is a simple Kafka Streams application which first takes a comma delimited string of userid and car colour from an input Kafka stream.  It then keeps only colours of "black", "blue" or "red". Finally, it gets the running count of the favourite colours overall and output this to a topic.

![Favourite Car Colour Streams Application](images/FavouriteCardColorSteramsApplication.png)


## Architecture

This Kafka Streams application is a simple ETL application.

It ***extracts*** data from an input Kafka topic named *fav-car-colour-input*.  It then ***transforms*** the data by counting valid colours.  Finally, it ***loads*** the result to an output Kafka topic named *fav-car-colour-output*.

|         Topic Name          |  Topic Type  |  Cleanup Policy  |   Key   |     Value     | Description                                              |
|:---------------------------:|:------------:|:----------------:|:-------:|:-------------:|:---------------------------------------------------------|
|    fav-car-colour-input     |    Input     | Delete (default) | User ID |    Colour     | Takes input from producer.                               |
| fav-car-colour-intermediary | Intermediary |     Compact      | User ID |    Colour     | As a table to keep up-to-date favourite colour by user.  |
|    fav-car-colour-output    |    Output    |     Compact      | Colour  | No. of Counts | As a table to keep up-to-date number of count by colour. |

## Development

### Run the application

1. Install Kafka.

2. Start a Kafka cluster using Kraft.

   * Format the storage directory:

    ```shell
    kafka-storage.sh random-uuid
    kafka-storage.sh format -t <uuid> -c ${KARKA_HOME}/config/kraft/server.properties
    ```

   * Start Kafka using Kraft

    ```shell
    kafka-server-start.sh config/kraft/server.properties
    ```

3. Create the topics.

    ```shell
    # Create input topic
    kafka-topics.sh --create --bootstrap-server localhost:29092 --replication-factor 1 --partitions 1 --topic fav-car-colour-input

    # Create intermediary topic
    kafka-topics.sh --create --bootstrap-server localhost:29092 --replication-factor 1 --partitions 1 --topic fav-car-colour-intermediary --config cleanup.policy=compact

    # Create output topic
    kafka-topics.sh --create --bootstrap-server localhost:29092 --replication-factor 1 --partitions 1 --topic fav-car-colour-output --config cleanup.policy=compact
    ```

5. List all topics for verifications.

    ```shell
   kafka-topics.sh --bootstrap-server localhost:29092 --list
   ```

6. Start a Console Consumer to read the transformation result.

    ```shell
    kafka-console-consumer.sh --bootstrap-server localhost:29092 \
        --topic fav-car-colour-output \
        --from-beginning \
        --formatter kafka.tools.DefaultMessageFormatter \
        --property print.key=true \
        --property print.value=true \
        --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
        --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
    ```

7. Start the Kafka Streams application as a Java application.
8. Start a Console Producer to send test message.

    ```shell
    kafka-console-producer.sh --broker-list localhost:29092 --topic fav-car-colour-input --property parse.key=true --property key.separator=,
    ```

9. Observe the results from the Console Consumer.


## Testing

### JUnit Test

Automatic JUnit Test Cases are written.  Just run the tests in Terminal by typing the following command:

```shell
mvn test
```

With the use of the [Kafka Streams Test Utilities](https://kafka.apache.org/documentation/streams/developer-guide/testing.html), unit tests can be run without a Kafka cluster, i.e. no need to create a real Kafka cluster before testing.

## Deployment

There are two ways to deploy this application.  One way is to package the application to a JAR file using Maven and run the Java console application in Terminal / Command Prompt.  The other way is to build a docker image and run it in docker environment.

### 1. Fat JAR deployment

1. This application can be packaged into a fat JAR file which includes all necessary dependencies using the following command:

    ```shell
    mvn clean package
    ```

2. To run the application,

    ```shell
    java -jar ./target/streams.fav-car-colour-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar
    ```

### 2. Docker image deployment

1. Build the docker image using the following command:

    ```shell
    docker build -f Dockerfile -t test-appsec .
    ```

2. In order that our container can communicate with Kafka, Kafka should be in the same docker environment.  Start Kafka cluster using ***docker-compose*** as follows:

    ```shell
    docker-compse up
    ```
   
3. Run the docker image using the following command:

    ```shell
    docker run --name test-appsec-app --network container:broker test-appsec:latest
    ```

## Acknowledgement
1. [Apache Kafka Series - Kafka Streams for Data Processing](https://www.udemy.com/course/kafka-streams/).
