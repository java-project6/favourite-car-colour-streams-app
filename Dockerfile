# Use the official maven/Jav 11 image to create a build artifact.
FROM maven:3-jdk-11-slim AS build-dev

# Set the working directory to /app
WORKDIR /app
# Copy the pom.xml file to download dependencies
COPY pom.xml ./
# Copy local code to the container image.
COPY src ./src

# Download depencies and build a release artifact
RUN mvn package -DskipTests


FROM openjdk:11-jre-slim

COPY --from=build-dev /app/src/main/resources /src/main/resources

# Copy the jar to the production image from the buildeer stage.
COPY --from=build-dev /app/target/streams.fav-car-colour-app-0.0.1-SNAPSHOT-jar-with-dependencies.jar /streams.fav-car-colour-app-0.0.1-SNAPSHOT.jar

# Run the console application on container startup.
CMD ["java", "-jar", "/streams.fav-car-colour-app-0.0.1-SNAPSHOT.jar"]


# ENTRYPOINT ["java","com.jaguarlandrover.telematics.streams.FavouriteCarApp"]