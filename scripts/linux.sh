# Format the storage directory:
bin/kafka-storage.sh random-uuid
bin/kafka-storage.sh format -t <uuid> -c ${KARKA_HOME}/config/kraft/server.properties

# Start Kafka using Kraft
bin/kafka-server-start.sh config/kraft/server.properties

# create input topic
bin/kafka-topics.sh --delete --bootstrap-server localhost:29092 --topic fav-car-colour-input
bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --replication-factor 1 --partitions 1 --topic fav-car-colour-input
bin/kafka-topics.sh --describe --bootstrap-server localhost:29092 --topic fav-car-colour-input

# create intermediary topic
bin/kafka-topics.sh --delete --bootstrap-server localhost:29092 --topic fav-car-colour-intermediary
bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --replication-factor 1 --partitions 1 --topic fav-car-colour-intermediary --config cleanup.policy=compact
bin/kafka-topics.sh --describe --bootstrap-server localhost:29092 --topic fav-car-colour-intermediary

# create out topic
bin/kafka-topics.sh --delete --bootstrap-server localhost:29092 --topic fav-car-colour-output
bin/kafka-topics.sh --create --bootstrap-server localhost:29092 --replication-factor 1 --partitions 1 --topic fav-car-colour-output --config cleanup.policy=compact
bin/kafka-topics.sh --describe --bootstrap-server localhost:29092 --topic fav-car-colour-output

# List all topics
bin/kafka-topics.sh --list --bootstrap-server localhost:29092

# start a consumer on the input topic
bin/kafka-console-consumer.sh --bootstrap-server localhost:29092 \
    --topic fav-car-colour-input \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.StringDeserializer

# start a consumer on the intermediary topic
bin/kafka-console-consumer.sh --bootstrap-server localhost:29092 \
    --topic fav-car-colour-intermediary \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.StringDeserializer

# start a consumer on the output topic
bin/kafka-console-consumer.sh --bootstrap-server localhost:29092 \
    --topic fav-car-colour-output \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer

# Launch the streams application

# start a kafka producer
bin/kafka-console-producer.sh --broker-list localhost:29092 --topic fav-car-colour-input --property parse.key=true --property key.separator=,


#
# Docker environment
#
docker-compose up
docker network list
docker network inspect test-appsec-pipeline_default

# Build Docker image
docker build -f Dockerfile -t test-appsec .

# Run Docker image
docker run --name test-appsec-app --network container:broker test-appsec:latest

