package dev.adafycheng.kafka.streams;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static org.junit.Assert.assertEquals;

public class FavouriteCarColourAppTest {

  TopologyTestDriver testDriver;
  StringSerializer stringSerializer = new StringSerializer();
  ConsumerRecordFactory<String, String> recordFactory = new ConsumerRecordFactory<>(stringSerializer, stringSerializer);
  StringDeserializer stringDeserializer = new StringDeserializer();
  LongDeserializer longDeserializer = new LongDeserializer();

  String inputTopicName = null;
  String intermediaryTopicName = null;
  String outputTopicName = null;

  @Before
  public void setUpTopologyTestDriver() throws IOException {
    Properties streamProps = new Properties();
    try (FileInputStream fis = new FileInputStream("src/test/resources/application.properties")) {
      streamProps.load(fis);
    }

    // Get the topic names from properties file.
    inputTopicName = streamProps.getProperty("application.input.topic");
    intermediaryTopicName = streamProps.getProperty("application.intermediary.topic");
    outputTopicName = streamProps.getProperty("application.output.topic");

    FavouriteCarColourApp app = new FavouriteCarColourApp(inputTopicName, intermediaryTopicName, outputTopicName);
    Topology topology = app.createTopology();
    testDriver = new TopologyTestDriver(topology, streamProps);
  }

  @After
  public void closeTestDriver() {
    testDriver.close();
  }

  public void pushNewInputRecord(String key, String value) {
    testDriver.pipeInput(recordFactory.create(inputTopicName, key, value));
  }

  public ProducerRecord<String, Long> readOutput() {
    return testDriver.readOutput(outputTopicName, new StringDeserializer(), new LongDeserializer());
  }

  @Test
  public void testDummy() {
    String dummy = "Du" + "mmy";
    assertEquals("Dummy", dummy);
  }

  @Test
  public void testSimpleCount() {
    String firstExampleKey = "ada";
    String firstExampleValue = "red";
    pushNewInputRecord(firstExampleKey, firstExampleValue);
    OutputVerifier.compareKeyValue(readOutput(), "red", 1L);
    Assert.assertNull(testDriver.readOutput(outputTopicName, stringDeserializer, longDeserializer));
  }

  @Test
  public void testMoreCount() {
    String testKey = "ada";
    String testValue = "red";
    pushNewInputRecord(testKey, testValue);
    OutputVerifier.compareKeyValue(readOutput(), "red", 1L);
    Assert.assertNull(testDriver.readOutput(outputTopicName, stringDeserializer, longDeserializer));

    testKey = "ada";
    testValue = "black";
    pushNewInputRecord(testKey, testValue);
    OutputVerifier.compareKeyValue(readOutput(), "red", 0L);
    OutputVerifier.compareKeyValue(readOutput(), "black", 1L);
    Assert.assertNull(testDriver.readOutput(outputTopicName, stringDeserializer, longDeserializer));
  }

}
