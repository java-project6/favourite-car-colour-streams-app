package dev.adafycheng.kafka.streams;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;


/**
 * This Kafka Streams application first takes a comma delimited topic of userid and car colour.
 * It then keeps only colours of "black", "blue" or "red".
 * Finally, it gets the running count of the favourite colours overall and output this to a topic.
 */
public class FavouriteCarColourApp {

  private final String inputTopicName;
  private final String intermediaryTopicName;
  private final String outputTopicName;

  public FavouriteCarColourApp(String inputTopicName, String intermediaryTopicName, String outputTopicName) {
    this.inputTopicName = inputTopicName;
    this.intermediaryTopicName = intermediaryTopicName;
    this.outputTopicName = outputTopicName;
  }

  public Topology createTopology() {

    StreamsBuilder builder = new StreamsBuilder();
    /*
     * 1 - Read from Kafka input topic,
     *     Filter the unwanted values,
     *     and write into an intermediary topic.
     */
    KStream<String, String> inputStream = builder
      .stream(inputTopicName, Consumed.with(Serdes.String(), Serdes.String()));

    KStream<String, String> intermediaryStream = inputStream.selectKey((key, color) -> key.trim())
      .mapValues(value -> value.trim().toLowerCase(Locale.ROOT))
      .filter((user, colour) -> Arrays.asList("black", "blue", "red").contains(colour));

    intermediaryStream.to(intermediaryTopicName);

    /*
     * 2 - Read from the intermediary topic as KTable
     *     Group by colours and count, and write into the final output topic.
     */
    KTable<String, String> intermediaryTable = builder.table(intermediaryTopicName);
    intermediaryTable.groupBy((user, color) -> new KeyValue<>(color, color))
      .count(Materialized.as("Counts"))
      .toStream()
      .to(outputTopicName, Produced.with(Serdes.String(), Serdes.Long()));

    return builder.build();
  }

  public static void main(String[] args) {
    // Read Application Properties
    Properties streamProps = new Properties();
    try (FileInputStream fis = new FileInputStream("src/main/resources/application.properties")) {
      streamProps.load(fis);
    } catch (IOException e) {
      System.exit(1);
    }

    // Disable the cache - not recommended in prod
    streamProps.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");

    String inputTopicName = streamProps.getProperty("application.input.topic");
    String intermediaryTopicName = streamProps.getProperty("application.intermediary.topic");
    String outputTopicName = streamProps.getProperty("application.output.topic");

    FavouriteCarColourApp app = new FavouriteCarColourApp(inputTopicName, intermediaryTopicName, outputTopicName);
    KafkaStreams streams = new KafkaStreams(app.createTopology(), streamProps);
    try (streams) {
      streams.cleanUp();  // Only do this in dev - not in prod
      streams.start();
    }

    // Shutdown hook to correctly close the streams application
    Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
  }

}
